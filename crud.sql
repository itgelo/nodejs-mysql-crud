/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : crud

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-08-31 12:25:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `mahasiswa`
-- ----------------------------
DROP TABLE IF EXISTS `mahasiswa`;
CREATE TABLE `mahasiswa` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_induk` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` text,
  PRIMARY KEY (`id`,`no_induk`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  UNIQUE KEY `no_induk` (`no_induk`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mahasiswa
-- ----------------------------
