var http = require("http");
var url = require("url");
var qString = require("querystring");
var router = require("routes")();
var view = require("swig");
var mysql = require("mysql");
var connection = mysql.createConnection({
    host     : "localhost",
    port     : 3306,
    database : "crud",
    user     : "root",
    password : ""
});

router.addRoute("/", function(req, res){
    var template = "";
    connection.query("select * from mahasiswa", function(err, rows, field){
        if(err) throw err;
        if(rows.length) template = "./views/index.html";
        else template = "./views/empty.html";
        var html = view.compileFile(template)({
            title      : "Crud",
            contentTle : "Data Mahasiswa",
            data       : rows
        });
        res.writeHead(200, {"Content-Type":"text/html"});
        res.end(html);
    });
});

router.addRoute("/insert", function(req, res){
    if(req.method.toUpperCase() == "POST"){
        var data_post = "";
        req.on("data", function(chunk){
            data_post += chunk;
        });

        req.on("end", function(){
            data_post = qString.parse(data_post);
            connection.query("insert into mahasiswa set ?", data_post,
                function(err, field){
                    if(err) throw err;
                    res.writeHead(302, {"Location": "/"});
                    res.end();
                }
            );
        });
    }else {
        var html = view.compileFile("./views/form.html")({
            title      : "crud - add data",
            contentTle : "Insert Data",
            disabled   : "",
            data       : ""
        });
        res.writeHead(200, {"Content-Type":"text/html"});
        res.end(html);
    }
});

router.addRoute("/update/:id?", function(req, res){
    connection.query("select * from mahasiswa where ?", {id: this.params.id},
        function(err, rows, field){
            if(rows.length){
                var data = rows[0];
                if(req.method.toUpperCase() == "POST"){
                    var data_post = "";
                    req.on("data", function(chunk){
                        data_post += chunk;
                    });

                    req.on("end", function(){
                        data_post = qString.parse(data_post);
                        connection.query("update mahasiswa set ? where ?", [
                            data_post,
                            { id : data.id } //where
                        ], function(err, field){
                            if(err) throw err;
                            res.writeHead(302, {"Location":"/"});
                            res.end();
                        });
                    });
                }else {
                    var html = view.compileFile("./views/form.html")({
                        title      : "crud - add data",
                        contentTle : "Update Data",
                        disabled   : "disabled",
                        data       : data
                    });
                    res.writeHead(200, {"Content-Type":"text/html"});
                    res.end(html);
                }
            }else {
                res.writeHead(404, {"Content-Type":"text/plain"});
                res.end("page not found!");
            }
        }
    );
});

router.addRoute("/delete/:id", function(req, res){
    connection.query("delete from mahasiswa where ?", {
        id : this.params.id
    }, function(err, field){
        if(err) throw err;
        res.writeHead(302, {"Location":"/"});
        res.end();
    });
});

http.createServer(function(req, res){
    var path = url.parse(req.url).pathname;
    var match = router.match(path);
    if(match){
        match.fn(req, res);
    }else {
        res.writeHead(404, {"Content-Type":"text/plain"});
        res.end("page not found!");
    }
}).listen(3000);